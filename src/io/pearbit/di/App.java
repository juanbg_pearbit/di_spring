package io.pearbit.di;

import java.nio.channels.ShutdownChannelGroupException;

import io.pearbit.di.knight.DragonKiller;
import io.pearbit.di.knight.Knight;
import io.pearbit.di.knight.impl.Guardian;
import io.pearbit.di.knight.impl.RescueDamsel;
import io.pearbit.di.quest.Quest;
import io.pearbit.di.quest.impl.ProtectCastle;
import io.pearbit.di.quest.impl.SavePrincess;

/**
 * This mini project show how, DI works, with a funny representation.
 * 
 * 
 * @author JuanBG
 *
 */
public class App {
	
	public static void main(String[] args) {
		App app = new App();
		app.showNoDependencyInjection();
		app.universalKnightFacotry();

	}

	/**
	 * This method show how the code works traditionally, it is hard coupled. Its difficult made corrections or updates. 
	 */
	public void showNoDependencyInjection() {
		DragonKiller knight = new DragonKiller(); //Oh yeah!, here we have our hero, this man, will kill the big Dragon for us.
		knight.embarkToMission(); //Yeah, he did it. but... wait a second. A princess need help!!. Go man!! Goooo!
		
		knight.embarkToMission();//wa... wait????, you need to rescue damsel!! come on bro!. 
		
		
		//The big problem here is that this knight was created only for kill dragons, he is a knight but he can change his behavior.
		//A solution for this could be, create a new one. a specialized knight to rescue people. and it will works, but what happen if you need another one and antoher one mission?
		//Create a hard code for all missions is difficult and a bad practice. for this. we have Inversion of control. 
		
		/**
		 * Before, the object has the responsibility to create its own dependencies, in this example, KillDragon creates its own quest.
		 * 
		 * but now we need revert it, and quit off the responsibility of create the dependencies to us. See the next method 
		 */
		
	}
	
	/**
	 * This method shows how, we create a knight and we, create for now, a knight specially to protect the castle.
	 */
	public void universalKnightFacotry() {
		
		Quest protectCastle = new ProtectCastle(); //We have a new mission!!!
		
		Knight knight = new Guardian(protectCastle); //Hey you bub, are you busy?, this is your new mission!
				
		knight.embarkToMission(); //YES SIR. 
		
		/**
		 * But... What what what. the princess is in dangerous. 
		 * Ajá! now out knight can be reassigned to other mission
		 * lets see 
		 */
		
		Quest savePrincess = new SavePrincess(); //I have a new mission!!!.
		
		knight = new RescueDamsel(savePrincess); //this is!, go go go
		
		knight.embarkToMission(); // YES SIR!
		
		/**
		 * Now the mission is completed, you can go to your normal work!
		 */
		
		knight = new Guardian(protectCastle); //Hey you bub, are you busy?, this is your new mission!
		
		knight.embarkToMission(); //YES SIR. 
	}
}

