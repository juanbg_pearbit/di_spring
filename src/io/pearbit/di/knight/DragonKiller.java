package io.pearbit.di.knight;

import io.pearbit.di.quest.impl.KillDragon;

/**
 * This is an useless knight, because it is hard core made only for kill dragons. 
 * @author JuanBG
 *
 */
public class DragonKiller {
	
	private KillDragon quest; //<- Hard code

	public DragonKiller() {
		super();
		this.quest = new KillDragon(); //this creates it own dependency. 
	}
	
	public void embarkToMission() {
		quest.embark();
	}
	
	
}
