package io.pearbit.di.knight;

/**
 * We create an universal knight, thought an interface we decide the main behavior, a knight will make a mission. we don't know how but this claim will be complied.
 * @author JuanBG
 *
 */
public interface Knight {
	
	public void embarkToMission();
}
