package io.pearbit.di.knight.impl;

import io.pearbit.di.knight.Knight;
import io.pearbit.di.quest.Quest;
/**
 * We adopt the behavior of a generic knight but now we know that we'll need an quest at the creation moment (We're inverting the dependency) we inject it in the O o = new O() statement.
 * @author JuanBG
 *
 */
public class Guardian implements Knight{

	Quest quest;
	
	public Guardian(Quest quest) {
		super();
		this.quest = quest;
	}



	@Override
	public void embarkToMission() {
		quest.embark();
	}

}
