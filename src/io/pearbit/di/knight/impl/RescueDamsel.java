package io.pearbit.di.knight.impl;

import io.pearbit.di.knight.Knight;
import io.pearbit.di.quest.Quest;

public class RescueDamsel implements Knight{
	Quest quest;
	
	public RescueDamsel(Quest quest) {
		super();
		this.quest = quest;
	}



	@Override
	public void embarkToMission() {
		quest.embark();
	}

}
