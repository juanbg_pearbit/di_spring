package io.pearbit.di.quest;

/**
 * We create a generic mission, only we know that it'll be emkarked, we dont know how.
 * @author JuanBG
 *
 */
public interface Quest {
	public void embark();
}
