package io.pearbit.di.quest.impl;

import io.pearbit.di.quest.Quest;

/**
 * This mission is explicit to kill a dragon 
 * @author JuanBG
 *
 */
public class KillDragon implements Quest{
	
	public void embark() {
		System.out.println("It's time to kill this Dragon!. ");
	}
}
