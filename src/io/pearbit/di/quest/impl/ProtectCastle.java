 package io.pearbit.di.quest.impl;

import io.pearbit.di.quest.Quest;

/**
 * In this case we know that this quest is specially for take care about a castle
 * @author JuanBG
 *
 */
public class ProtectCastle  implements Quest{

	@Override
	public void embark() {
		System.out.println("All Righ, we will protect this place!.");
	}

}
