package io.pearbit.di.quest.impl;

import io.pearbit.di.quest.Quest;

/**
 * In this case we know that this quest is specially for take care about a castle
 * @author JuanBG
 *
 */
public class SavePrincess implements Quest{

	@Override
	public void embark() {
		System.out.println("All Righ, I'm going princess don't worry!!.");
	}

}
